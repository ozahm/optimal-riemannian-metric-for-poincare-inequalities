classdef OptimalWeightedPoincare_2D_linear
    
    properties
        
        TargetMeasure
        
        %%% Parameters...
        parametrization
        normalizeIterates
        StepSizeAdaptation
        gamma
        
        %%% Regularizing the "min" in the objectif function
        Jp_type
        Jp_param
        
        %%% Handeling the normalization in the objective function
        Jn_type
        Jn_param
        
        %%% Chosing the scalar product
        RiemanianGradient
    end
    
    
    
    methods
        
        function obj = OptimalWeightedPoincare_2D_linear(TM)
            
            obj.TargetMeasure = TM;
            
            obj.Jn_type = 'SelfNormalized';
            obj.Jn_param = [];
            
            obj.Jp_type = 'Original';
%             obj.Jp_type = 'SoftMinAdaptive';
%             obj.Jp_type = 'SoftMin';
%             obj.Jp_type = 'Sum';
            obj.Jp_param = [];
            
            obj.parametrization = 'VV';
            obj.normalizeIterates = 1;
            obj.RiemanianGradient = 0;
            obj.StepSizeAdaptation = 1;
            
        end
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        function [Lambda,u,duduT,flag] = Eigendecomp(obj,W,kmax)
            
            if nargin<3
                kmax = 15;
            end
            K = obj.TargetMeasure.Stiffness( W );
            K = (K+K')/2;
            
            % Solve the eigenvalue problem
            [u,Lambda,flag] = eigs(K,obj.TargetMeasure.Mmu,kmax+1,'smallestabs','IsSymmetricDefinite',true,'Tolerance',1e-16);
            Lambda = diag(Lambda);
            Lambda = Lambda(2:end);
            u = u(:,2:end);
            
            % Compute the gradient of lambda w.r.t. W
            duduT = cell(kmax,1);
            for i=1:kmax
                gradu = obj.TargetMeasure.gradientOfField(u(:,1));
                duduT{i} = MatrixField2x2( [gradu(:,1).^2 , gradu(:,1).*gradu(:,2) , gradu(:,1).*gradu(:,2) , gradu(:,2).^2] );
            end
            
        end
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        function [Jval,gradW,Lambda,u,flag] = Jn(obj,W)
            
            
            switch obj.Jn_type
                case 'None'
                    [Jval,gradW,Lambda,u,flag] = Jp(obj,W);
%                     dmu = obj.TargetMeasure.ElementSurface'.*obj.TargetMeasure.mu;
%                     trGradW = gradW.trace()'*dmu;
%                     Id = MatrixField2x2.eye(obj.TargetMeasure.nElem);
%                     gradW = gradW.plus(Id.mtimes(-trGradW));
                    
                case 'SelfNormalized'
                    % Normalize W
                    dmu = obj.TargetMeasure.ElementSurface'.*obj.TargetMeasure.mu;
                    trW = W.trace()'*dmu;
                    trC = trace(obj.TargetMeasure.CovMu);
                    
                    [Jval,gradW,Lambda,u,flag] = Jp(obj, W.mtimes(trC/trW) );
                    
                    trGW = gradW.times(W);
                    trGW = trGW.trace()'*dmu;
                    Id = MatrixField2x2.eye(obj.TargetMeasure.nElem);
                    
                    gradW = gradW.plus(Id.mtimes(-trGW/trW));
                    gradW = gradW.mtimes(trC/trW);
                    
                case 'Penalization'
                    
                    [Jval,gradW,Lambda,u,flag] = Jp(obj,W);
                    
                    dmu = obj.TargetMeasure.ElementSurface'.*obj.TargetMeasure.mu;
                    trW = W.trace()'*dmu;
                    trC = trace(obj.TargetMeasure.CovMu);
                    Id = MatrixField2x2.eye(obj.TargetMeasure.nElem);
                    
                    Jval = Jval - obj.Jn_param*(trW-trC)^2/2;
                    gradW = gradW.plus(Id.mtimes( - obj.Jn_param*(trW-trC) ));
            end
            
            
            
            
            if obj.RiemanianGradient
                gradW = W.times(gradW.times(W));
%                 W12 = sqrt(W);
%                 gradW = W12.times(gradW.times(W12));
            end
            
            
            
        end
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        function [Jval,gradW,Lambda,u,flag] = Jp(obj,W)
            
            [Lambda,u,duduT,flag] = Eigendecomp(obj,W);
            
            switch obj.Jp_type
                case 'Original'
                    gradW = duduT{1};
                    Jval = Lambda(1);
                    
                case 'SoftMin'
                    
                    w = exp(-obj.Jp_param*(Lambda-Lambda(1)));
                    sumw = sum(w);
                    Jval = Lambda(1)-log(sumw)/obj.Jp_param;
                    
                    gradW = duduT{1}.data;
                    for i=2:length(duduT)
                        gradW = gradW + (w(i)/sumw)*duduT{i}.data;
                    end
                    gradW = MatrixField2x2(gradW);
                    
                case 'Sum'
                    Jval = Lambda(1);
                    gradW = duduT{1};
                    for i=2:obj.Jp_param
                        Jval = Jval + Lambda(i);
                        gradW = gradW.plus(duduT{i});
                    end
                    
                case 'SoftMinAdaptive'
                    
                    p = abs(Lambda(1)-1.001)^(-obj.Jp_param);
                    
                    w = exp(-p*(Lambda-Lambda(1)));
                    sumw = sum(w);
                    Jval = Lambda(1)-log(sumw)/p;
                    
                    gradW = duduT{1}.data;
                    for i=2:length(duduT)
                        gradW = gradW + (w(i)/sumw)*duduT{i}.data;
                    end
                    gradW = MatrixField2x2(gradW);
            end
            
            
        end
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        function [V,Lambda,f,Jval,delta,toplot] = OptimalMatrixField(obj,V,kmax,delta,dispflag,toplot)
            
            %%% Initialize
            if nargin<2 || isempty(V)
                V = MatrixField2x2.eye(obj.TargetMeasure.nElem);
            end
            % Number of iterations
            if nargin<3 || isempty(kmax)
                kmax = 30; 
            end
            % Step size
            if nargin<4 || isempty(delta)
                delta = 0.01; 
            end
            % Display...
            if nargin<5 || isempty(dispflag)
                dispflag = 1;
            end
            if nargin<6 || isempty(toplot)
                toplot = [];
                toplot.Lambda = [];
                toplot.J = [];
            end
            dmu = obj.TargetMeasure.ElementSurface'.*obj.TargetMeasure.mu;
            CovMu = obj.TargetMeasure.CovMu;
            
            
            omega = 1.25;
            
            
            W = Wof(V,obj);
            
            if obj.normalizeIterates
                %%% Normalize
                Z = trace(E( W ,dmu))/trace(CovMu);
                W = W.mtimes( 1/Z );
                V = normalize(V,Z,obj);
            end
            
            % Initial evaluation
            [Jval,dW,Lambda,f] = Jn(obj,W);
            
            % Displays
            if dispflag
                
                normdW = sum(dW.data.^2,2)'*dmu;
                disp('+--------------------------------------+')
                disp('|iter|   J       | Poincaré  |    ||dW ||   | StepSize |')
                disp(['| ' num2str(1) ' |   ' num2str( Jval , '%.3f') '    |   ' num2str( (W.trace()'*dmu)/(trace(CovMu)*Lambda(1,end)), '%.3f') '    | ' num2str( normdW, '%.5f') '          |' num2str(delta,'%.3e') ])
                
                %%% Plot the (inverse) spectrum
                toplot.Lambda = [toplot.Lambda, Lambda(1:min(10,size(Lambda,1)),end)];
                plot(toplot.Lambda')
                hold on
                plot([1,size(toplot.Lambda,2)],[1,1],'k--' )
                
                %%% Plot the cost function
                toplot.J = [toplot.J, Jval];
                plot(toplot.J, 'k')
                hold off
            end
            
            for k=2:kmax
                
                if obj.normalizeIterates
                    % Normalize
                    W = Wof(V,obj);
                    Z = trace(E( W ,dmu))/trace(CovMu);
                    W = W.mtimes( 1/Z );
                    V = normalize(V,Z,obj);
                end
                
                % Compute Vk+1
                dV = dVof(dW,V,obj);
                V0 = V.plus(dV.mtimes(delta));
                V1 = V.plus(dV.mtimes(delta*omega));
                V2 = V.plus(dV.mtimes(delta/omega));
                
                if obj.normalizeIterates
                    % Normalize
                    W0 = Wof(V0,obj);
                    Z0 = trace(E( W0 ,dmu))/trace(CovMu);
                    V0 = normalize(V0,Z0,obj);
                    
                    W1 = Wof(V1,obj);
                    Z1 = trace(E( W1 ,dmu))/trace(CovMu);
                    V1 = normalize(V1,Z1,obj);
                    
                    W2 = Wof(V2,obj);
                    Z2 = trace(E( W2 ,dmu))/trace(CovMu);
                    V2 = normalize(V2,Z2,obj);
                    
                end
                
                % Evaluate
                [J0,dW0,Lambda0] = Jn(obj,Wof(V0,obj));
                [J1,dW1,Lambda1] = Jn(obj,Wof(V1,obj));
                [J2,dW2,Lambda2] = Jn(obj,Wof(V2,obj));
                
                % Select the optimal over {V0,V1,V2}
                if J0 >= max([J1,J2,Jval(end)])
                    V = V0;
                    dW = dW0;
                    Lambda = [Lambda,Lambda0];
                    Jval = [Jval,J0];
                elseif J1 >= max([J0,J2,Jval(end)])
                    V = V1;
                    dW = dW1;
                    delta = delta*omega;
                    Lambda = [Lambda,Lambda1];
                    Jval = [Jval,J1];
                elseif J2 >= max([J0,J1,Jval(end)])
                    V = V2;
                    dW = dW2;
                    delta = delta/omega;
                    Lambda = [Lambda,Lambda2];
                    Jval = [Jval,J2];
                elseif Jval(end) >= max([J0,J1,J2])
                    delta = delta/(omega^2);
                    Lambda = [Lambda,Lambda(:,end)];
                    Jval = [Jval,Jval(end)];
                end
                
                % Displays
                if dispflag
                    normdW = sum(dW.data.^2,2)'*dmu;
                    disp(['| ' num2str(k) ' |   ' num2str( Jval(end) , '%.3f') '    |   ' num2str( (W.trace()'*dmu)/(trace(CovMu)*Lambda(1,end)), '%.3f') '    | ' num2str( normdW, '%.5f') '          |' num2str(delta,'%.3e') ])
                    
                    %%% Plot the (inverse) spectrum
                    toplot.Lambda = [toplot.Lambda, Lambda(1:min(5,size(Lambda,1)),end)];
                    plot(toplot.Lambda')
                    hold on
                    plot([1,size(toplot.Lambda,2)],[1,1],'k--' )
                    
                    %%% Plot the cost function
                    toplot.J = [toplot.J, Jval(end)];
                    plot(toplot.J, 'k')
                    pause(0.01)
                    hold off
                    
                end
            end
            
        end
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        function [V,Lambda,f,Jval,delta,toplot] = OptimalMatrixField_NAG(obj,V,kmax,delta0,dispflag,toplot)
            
            %%% Initialize
            if nargin<2 || isempty(V)
                V = MatrixField2x2.eye(obj.TargetMeasure.nElem);
            end
            % Number of iterations
            if nargin<3 || isempty(kmax)
                kmax = 30; 
            end
            % Step size
            if nargin<4 || isempty(delta0)
                delta0 = 0.01; 
            end
            % Display...
            if nargin<5 || isempty(dispflag)
                dispflag = 1;
            end
            if nargin<6 || isempty(toplot)
                toplot = [];
                toplot.Lambda = [];
                toplot.J = [];
            end
            dmu = obj.TargetMeasure.ElementSurface'.*obj.TargetMeasure.mu;
            CovMu = obj.TargetMeasure.CovMu;
            
            
            
            W = Wof(V,obj);
            
            if obj.normalizeIterates
                %%% Normalize
                Z = trace(E( W ,dmu))/trace(CovMu);
                W = W.mtimes( 1/Z );
                V = normalize(V,Z,obj);
            end
            
            % Initial evaluation
            [Jval,dW,Lambda,f] = Jn(obj,W);
            delta = delta0;
            tau_old = 0;
            G_old = V;

            % Displays
            if dispflag
                
                normdW = sum(dW.data.^2,2)'*dmu;
                disp('+--------------------------------------+')
                disp('|iter|   J       | Poincaré  |    ||dW ||   | StepSize |')
                disp(['| ' num2str(1) ' |   ' num2str( Jval , '%.3f') '    |   ' num2str( (W.trace()'*dmu)/(trace(CovMu)*Lambda(1,end)), '%.3f') '    | ' num2str( normdW, '%.5f') '          |' num2str(delta,'%.3e') ])
                
                %%% Plot the (inverse) spectrum
                toplot.Lambda = [toplot.Lambda, Lambda(1:min(10,size(Lambda,1)),end)];
                plot(toplot.Lambda')
                hold on
                plot([1,size(toplot.Lambda,2)],[1,1],'k--' )
                
                %%% Plot the cost function
                toplot.J = [toplot.J, Jval];
                plot(toplot.J, 'k')
                hold off
            end
            

            for k=2:kmax
                % 
                tau_new = ( 1+sqrt(1+4*tau_old^2) )/2;
                gamma_k = (1-tau_old)/tau_new;
                %
                delta = delta0/sqrt(1+k);
                % Compute Vk
                dV = dVof(dW,V,obj);
                G_new = V.plus(dV.mtimes(delta));
                V = G_new.mtimes(1-gamma_k);
                V = V.plus(G_old.mtimes(gamma_k));
                
                % update
                tau_old = tau_new;
                G_old = G_new;

                if obj.normalizeIterates
                    % Normalize
                    W = Wof(V,obj);
                    Z = trace(E( W ,dmu))/trace(CovMu);
                    V = normalize(V,Z,obj);
                end
                
                % Evaluate
                [Jnew,dW,LambdaNew] = Jn(obj,Wof(V,obj));
                
                Lambda = [Lambda,LambdaNew];
                Jval = [Jval,Jnew];
                
                % Displays
                if dispflag
                    normdW = sum(dW.data.^2,2)'*dmu;
                    disp(['| ' num2str(k) ' |   ' num2str( Jval(end) , '%.3f') '    |   ' num2str( (W.trace()'*dmu)/(trace(CovMu)*Lambda(1,end)), '%.3f') '    | ' num2str( normdW, '%.5f') '          |' num2str(delta,'%.3e') ])
                    
                    %%% Plot the (inverse) spectrum
                    toplot.Lambda = [toplot.Lambda, Lambda(1:min(10,size(Lambda,1)),end)];
                    plot(toplot.Lambda')
                    hold on
                    plot([1,size(toplot.Lambda,2)],[1,1],'k--' )
                    
                    %%% Plot the cost function
                    toplot.J = [toplot.J, Jval(end)];
                    plot(toplot.J, 'k')
                    pause(0.01)
                    hold off
                    
                end
            end
            
        end
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        function [V,Lambda,f,Jval,delta,toplot] = OptimalMatrixField_MOMENTUM(obj,V,kmax,delta,dispflag,toplot)
            
            %%% Initialize
            if nargin<2 || isempty(V)
                V = MatrixField2x2.eye(obj.TargetMeasure.nElem);
            end
            % Number of iterations
            if nargin<3 || isempty(kmax)
                kmax = 30; 
            end
            % Step size
            if nargin<4 || isempty(delta)
                delta = 0.01; 
            end
            % Display...
            if nargin<5 || isempty(dispflag)
                dispflag = 1;
            end
            if nargin<6 || isempty(toplot)
                toplot = [];
                toplot.Lambda = [];
                toplot.J = [];
            end
            dmu = obj.TargetMeasure.ElementSurface'.*obj.TargetMeasure.mu;
            CovMu = obj.TargetMeasure.CovMu;
            
            
            
            W = Wof(V,obj);
            
            if obj.normalizeIterates
                %%% Normalize
                Z = trace(E( W ,dmu))/trace(CovMu);
                W = W.mtimes( 1/Z );
                V = normalize(V,Z,obj);
            end
            
            % Initial evaluation
            [Jval,dW,Lambda,f] = Jn(obj,W);
            G = dVof(dW,V,obj);
            G = G.mtimes(delta);
            
            % Displays
            if dispflag
                
                normdW = sum(dW.data.^2,2)'*dmu;
                disp('+--------------------------------------+')
                disp('|iter|   J       | Poincaré  |    ||dW ||   | StepSize |')
                disp(['| ' num2str(1) ' |   ' num2str( Jval , '%.3f') '    |   ' num2str( (W.trace()'*dmu)/(trace(CovMu)*Lambda(1,end)), '%.3f') '    | ' num2str( normdW, '%.5f') '          |' num2str(delta,'%.3e') ])
                
                %%% Plot the (inverse) spectrum
                toplot.Lambda = [toplot.Lambda, Lambda(1:min(10,size(Lambda,1)),end)];
                plot(toplot.Lambda')
                hold on
                plot([1,size(toplot.Lambda,2)],[1,1],'k--' )
                
                %%% Plot the cost function
                toplot.J = [toplot.J, Jval];
                plot(toplot.J, 'k')
                hold off
            end
            
            for k=2:kmax
                
                % Compute Vk+1
                dV = dVof(dW,V,obj);
                
                
                G = G.mtimes( obj.gamma );
                
                if ~obj.StepSizeAdaptation
                    % No step-size adaptation
                    G = G.plus(dV.mtimes(delta));
                    V = V.plus(G);
                    
                    if obj.normalizeIterates
                        % Normalize
                        W = Wof(V,obj);
                        Z = trace(E( W ,dmu))/trace(CovMu);
                        V = normalize(V,Z,obj);
                    end
                    
                    % Evaluate
                    [Jnew,dW,LambdaNew] = Jn(obj,Wof(V,obj));
                    Lambda = [Lambda,LambdaNew];
                    Jval = [Jval,Jnew];
                    
                else
                    % Step-size adaptation
                    omega = 1.25;
                    
                    G0 = G.plus(dV.mtimes(delta));
                    G1 = G.plus(dV.mtimes(delta*omega));
                    G2 = G.plus(dV.mtimes(delta/omega));
                    V0 = V.plus(G0);
                    V1 = V.plus(G1);
                    V2 = V.plus(G2);
                    
                    if obj.normalizeIterates
                        % Normalize
                        W0 = Wof(V0,obj);
                        Z0 = trace(E( W0 ,dmu))/trace(CovMu);
                        V0 = normalize(V0,Z0,obj);
                        
                        W1 = Wof(V1,obj);
                        Z1 = trace(E( W1 ,dmu))/trace(CovMu);
                        V1 = normalize(V1,Z1,obj);
                        
                        W2 = Wof(V2,obj);
                        Z2 = trace(E( W2 ,dmu))/trace(CovMu);
                        V2 = normalize(V2,Z2,obj);
                    end
                    
                    % Evaluate
                    [J0,dW0,Lambda0] = Jn(obj,Wof(V0,obj));
                    [J1,dW1,Lambda1] = Jn(obj,Wof(V1,obj));
                    [J2,dW2,Lambda2] = Jn(obj,Wof(V2,obj));
                    
                    % Select the best of V0,V1,V2
                    if J0 >= max([J1,J2])
                        V = V0;
                        dW = dW0;
                        Lambda = [Lambda,Lambda0];
                        Jval = [Jval,J0];
                    elseif J1 >= max([J0,J2])
                        V = V1;
                        dW = dW1;
                        delta = delta*omega;
                        Lambda = [Lambda,Lambda1];
                        Jval = [Jval,J1];
                    elseif J2 >= max([J0,J1])
                        V = V2;
                        dW = dW2;
                        delta = delta/omega;
                        Lambda = [Lambda,Lambda2];
                        Jval = [Jval,J2];
                    end
                    
                end
                
                % Displays
                if dispflag
                    normdW = sum(dW.data.^2,2)'*dmu;
                    disp(['| ' num2str(k) ' |   ' num2str( Jval(end) , '%.3f') '    |   ' num2str( (W.trace()'*dmu)/(trace(CovMu)*Lambda(1,end)), '%.3f') '    | ' num2str( normdW, '%.5f') '          |' num2str(delta,'%.3e') ])
                    
                    %%% Plot the (inverse) spectrum
                    toplot.Lambda = [toplot.Lambda, Lambda(1:min(10,size(Lambda,1)),end)];
                    plot(toplot.Lambda')
                    hold on
                    plot([1,size(toplot.Lambda,2)],[1,1],'k--' )
                    
                    %%% Plot the cost function
                    toplot.J = [toplot.J, Jval(end)];
                    plot(toplot.J, 'k')
                    pause(0.01)
                    hold off
                    
                end
            end
            
        end
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        function [W,Lambda,f,Jval,delta,toplot] = OptimalMatrixField_geodesic(obj,W,kmax,delta,type,paramJ,dispflag,toplot,path)
%             warning ('off','all')
            
            %%% Parameters
            % Number of iterations
            if nargin<3 || isempty(kmax)
                kmax = 30; 
            end
            % Step size
            if nargin<4 || isempty(delta)
                delta = 0.01; 
            end
            % Type
            if nargin<5 || isempty(type)
                type = 'SoftMin';
            end
            % Type parameter
            if nargin<6 || isempty(paramJ)
                paramJ = 1; 
            end
            % Display...
            if nargin<7 || isempty(dispflag)
                dispflag = 1;
            end
            if nargin<8 || isempty(toplot)
                toplot = [];
                toplot.Lambda = [];
                toplot.J = [];
            end
            if nargin<9 || isempty(path)
                path = 'VV';
            end
            
            omega = 1.5;
            
            %%% Precomputes{
            dmu = obj.TargetMeasure.ElementSurface'.*obj.TargetMeasure.mu;
            CovMu = obj.TargetMeasure.CovMu;
            %%% Precomputes}
            
            %%% Initialize
            if nargin<2 || isempty(W)
                W = MatrixField2x2.eye(obj.TargetMeasure.nElem);
            end
            
            %%% Normalize
            Z = trace(E( W ,dmu));
            W = W.mtimes( trace(CovMu)/Z );
            % Initial evaluation
            [Jval,gradW,Lambda,f] = J(obj,W,type,paramJ);
%             Jval = Lambda(1,end); %%%
            
            % Displays
            if dispflag
                
                distToCov = norm( E(W,dmu) - CovMu )/norm( CovMu );
                disp('+--------------------------------------+')
                disp('|iter| Poincaré  | ||E[W]-CovMu|| | StepSize |')
                disp(['| ' num2str(1) ' |   ' num2str( 1/(Lambda(1,end)), '%.3f') '    | ' num2str( distToCov, '%.5f') '          |' num2str(delta,'%.3e') ])
                
                %%% Plot the (inverse) spectrum
                toplot.Lambda = [toplot.Lambda, Lambda(1:min(5,size(Lambda,1)),end)];
                plot(toplot.Lambda')
                hold on
                plot([1,size(toplot.Lambda,2)],[1,1],'k--' )
                
                %%% Plot the cost function
                toplot.J = [toplot.J, Jval];
                plot(toplot.J, 'k')
                hold off
            end
            
            for k=2:kmax
                
                % Normalize
                Z = trace(E( W ,dmu));
                W = W.mtimes( trace(CovMu)/Z );
                
                % Compute the geodesic paths
                W0 = W.geodesicSPD_tangent(gradW,delta,path);
                W1 = W.geodesicSPD_tangent(gradW,delta*omega,path);
                W2 = W.geodesicSPD_tangent(gradW,delta/omega,path);
                
                
                if (~isreal(W0.data)) || (~isreal(W1.data)) || (~isreal(W2.data))
                    warning('Complex valued here!!')
                end
                
                
                % Evaluate
                [J0,gradW0,Lambda0] = J(obj,W0,type,paramJ);
                [J1,gradW1,Lambda1] = J(obj,W1,type,paramJ);
                [J2,gradW2,Lambda2] = J(obj,W2,type,paramJ);
                
                % Select the optimal over {V0,V1,V2}
                if J0 >= max([J1,J2,Jval(end)])
                    W = W0;
                    gradW = gradW0;
                    Lambda = [Lambda,Lambda0];
                    Jval = [Jval,J0];
                elseif J1 >= max([J0,J2,Jval(end)])
                    W = W1;
                    gradW = gradW1;
                    delta = delta*omega;
                    Lambda = [Lambda,Lambda1];
                    Jval = [Jval,J1];
                elseif J2 >= max([J0,J1,Jval(end)])
                    W = W2;
                    gradW = gradW2;
                    delta = delta/omega;
                    Lambda = [Lambda,Lambda2];
                    Jval = [Jval,J2];
                elseif Jval(end) >= max([J0,J1,J2])
                    delta = delta/(omega^2);
                    Lambda = [Lambda,Lambda(:,end)];
                    Jval = [Jval,Jval(end)];
                end
                
                % Displays
                if dispflag
                    distToCov = norm( E(W,dmu) - CovMu )/norm( CovMu );
                    disp(['| ' num2str(k) ' |   ' num2str( 1/(Lambda(1,end)), '%.3f') '    | ' num2str( distToCov, '%.5f') '          |' num2str(delta,'%.3e') ])
                    
                    %%% Plot the (inverse) spectrum
                    toplot.Lambda = [toplot.Lambda, Lambda(1:min(5,size(Lambda,1)),end)];
                    plot(toplot.Lambda')
                    hold on
                    plot([1,size(toplot.Lambda,2)],[1,1],'k--' )
                    
                    %%% Plot the cost function
                    toplot.J = [toplot.J, Jval(end)];
                    plot(toplot.J, 'k')
                    pause(0.01)
                    hold off
                    
                end
            end
            
        end
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        function [ngradf2,gradf] = normGradf2(obj,f)
            
            [p,~,t] = meshToPet(obj.TargetMeasure.Model.Mesh);
            [gradx,grady] = pdegrad(p,t,f);
            gradf = [gradx;grady]';
            ngradf2 = sum((gradf*obj.CovMu).*gradf,2);
            
        end
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        function nu = Gaussian(obj)
            
            J = (obj.ElementCenter' - obj.MeanMu);
            J = (J/obj.CovMu).*J ;
            J = sum(J,2);
            nu = exp(-J/2);
            
            % Normalize
            Z = obj.ElementSurface*nu;
            nu = nu/Z;
        end
        
        

    end
end