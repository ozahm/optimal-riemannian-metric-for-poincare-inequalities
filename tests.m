clear all
%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%      Test case                 %%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

type = 'Trimodal';
% hmax = 0.02;
hmax = 0.04;
% hmax = 0.06;
kmax = 50;
LargerSupport = 0;

%%
type = 'H';
hmax = 0.02;
kmax = 50;
LargerSupport = 0;

%%
type = 'H';
hmax = 0.02;
kmax = 50;
LargerSupport = -0.0000001;

%%
type = 'Ring';
hmax = 0.03;
kmax = 30;
LargerSupport = 0;

%% Plot to show the discretization

close all

TM = TargetMeasure(type,LargerSupport,0.04,hmax);

switch type
    case 'H'
        if LargerSupport<0
            type = 'H_Hull';
        end
end


data = TM.mu;

tmp = repmat(data(:)',3,1);
[p,e,t] = meshToPet(TM.Model.Mesh);
t = t(1:3,:);
x = p(1,:);
y = p(2,:);
P = [x(t(:));y(t(:))];
T = reshape(1:size(P,2),[3 size(P,2)/3]);
h = trisurf(T',P(1,:),P(2,:),tmp(:));
colormap cool
set(h,'edgecolor','k');
h.LineWidth=0.01;

axis equal
set(gca, 'box','off','XTickLabel',[],'XTick',[],'YTickLabel',[],'YTick',[])
axis off


grid off
view(0,90)


axis equal
pause(0.5)
% print(['plots/' type '_pdf'], '-dpng','-r300');

%% Initilize

close all

figure(1)
TM.plotMesh()

pause(0.5)
% print(['plots/' type '_mesh'], '-dpng','-r400');

figure(2)
TM.plot(TM.mu)
axis equal
pause(0.5)
% print(['plots/' type '_pdf'], '-dpng','-r400');

obj = OptimalWeightedPoincare_2D_linear(TM);
obj.Jn_type = 'SelfNormalized';
obj.normalizeIterates = 1;
obj.RiemanianGradient = 0;
obj.Jp_type = 'Original'; obj.Jp_param = 0;
obj.parametrization = 'VV';
warning ('off','all')

axis equal


%% Plot the first modes

Wid = MatrixField2x2.eye(obj.TargetMeasure.nElem);
dmu = obj.TargetMeasure.ElementSurface'.*obj.TargetMeasure.mu;
CovMu = obj.TargetMeasure.CovMu;
Z = trace(E( Wid ,dmu))/trace(CovMu);
Wid = Wid.mtimes( 1/Z );
[Jval,gradW,Lambda,u,flag] = Jp(obj,Wid) ;


for i=1:6
TM.plot(u(:,i))

axis equal
set(gca, 'box','off','XTickLabel',[],'XTick',[],'YTickLabel',[],'YTick',[])
axis off
pause(0.5)
% print(['plots/' type '_Modes_' num2str(i)], '-dpng','-r200');

end

%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%      Run the optimization      %%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Gradient ascent (with stepsize adaptation)
algorithm = 'Gradient ascent';
figure(1)
clf

obj.StepSizeAdaptation = 0;
obj.gamma = 0;
delta = 0.01;
[V,D,f,Jval,delta,toplot] = OptimalMatrixField_MOMENTUM(obj,[],kmax,delta,1,[]);
% [V,D,f,Jval,delta,toplot] = OptimalMatrixField_MOMENTUM(obj,V,kmax,delta,1,toplot);

%% MOMENTUM
algorithm = 'Momentum';
figure(1)
clf

obj.StepSizeAdaptation = 0;
obj.gamma = 0.5;
delta = 0.01;
[V,D,f,Jval,delta,toplot] = OptimalMatrixField_MOMENTUM(obj,[],kmax,delta,1,[]);
% [V,D,f,Jval,delta,toplot] = OptimalMatrixField_MOMENTUM(obj,V,kmax,delta,1,toplot);

%% NAG
algorithm = 'NAG';
figure(2)
clf

delta = 0.01;
[V,D,f,Jval,delta,toplot] = OptimalMatrixField_NAG(obj,[],kmax,delta,1,[]);
% [V,D,f,Jval,delta,toplot] = OptimalMatrixField_NAG(obj,V,kmax,delta,1,toplot);

%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%      Plot                      %%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% Plot the first modes AFTER optimization

W = Wof(V,obj);
dmu = obj.TargetMeasure.ElementSurface'.*obj.TargetMeasure.mu;
CovMu = obj.TargetMeasure.CovMu;
Z = trace(E( W ,dmu))/trace(CovMu);
W = W.mtimes( 1/Z );
[Jval,gradW,Lambda,u,flag] = Jp(obj,W) ;


for i=1:6
    uplot = u(:,i);
    q = 0.02;
    u1 = quantile(uplot,1-q);
    u0 = quantile(uplot,q);
    uplot(uplot>u1) = u1;
    uplot(uplot<u0) = u0;
    TM.plot(uplot)

    axis equal
    set(gca, 'box','off','XTickLabel',[],'XTick',[],'YTickLabel',[],'YTick',[])
    axis off
    pause(1)
%     print(['plots/' type '_ModesOptimal_' num2str(i)], '-dpng','-r200');

end



%% plot convergence TIKZ

clf
hl = semilogy(toplot.Lambda(1:5,:)','LineWidth',1.2);
hold on
semilogy(ones(size(toplot.Lambda',1),1),':k')
hold off
xlabel('Iteration number')
ylabel('Spectrum')
axis([0,50,1e-1,20])
legend({'$\lambda_2$','$\lambda_3$','$\lambda_4$','$\lambda_5$','$\lambda_6$'},...
    'Location','southeast')
title(algorithm)


%% Plot the trace of W

W = Wof(V,obj);



figure(11)
TM.plot(log(trace(W))/log(10),1)
axis square
% pause(0.5)
% print(['plots/' type '_traceW'], '-dpng','-r200');

figure(10)
TM.plotW(W,21,0.03)
axis square
% pause(0.5)
% print(['plots/' type '_traceW_ellipse'], '-dpng','-r200');

figure(12)
TM.plotDrift(MatrixField2x2.eye(TM.nElem),100)

axis square
% pause(0.5)
% print(['plots/' type '_driftId'], '-dpng','-r200');

figure(13)
TM.plotDrift(W,50)
axis square
% pause(0.5)
% print(['plots/' type '_driftW'], '-dpng','-r200');






%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%       Langevin dynamic         %%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
W = Wof(V,obj);

X0 = [0;0.65];
N = 5000;
dt = 0.015;
X = TM.Langevin(X0,N,dt);
Y = TM.Langevin(X0,N,dt,W);
Z = TM.Langevin_MH(X0,N,dt,W);

%%
figure(1)
TM.plot(TM.mu)
hold on
plot(X(:,1),X(:,2),'Color',[0,0,0, 0.2],'linewidth',1.5,'Marker','.')
hold off
axis square
% pause(2)
% print(['plots/' type '_SDE_dt' num2str(dt) '.png'], '-dpng','-r300');


figure(2)
TM.plot(TM.mu)
hold on
plot(Y(:,1),Y(:,2),'Color',[0,0,1, 0.2],'linewidth',1.5,'Marker','.')
hold off
axis square
% pause(2)
% print(['plots/' type '_SDE_W_dt' num2str(dt) '.png'], '-dpng','-r300');

figure(3)
TM.plot(TM.mu)
hold on
plot(Z(:,1),Z(:,2),'Color',[1,0,1, 0.2],'linewidth',1.5,'Marker','.')
hold off
axis square
% pause(1)
% print(['plots/' type '_SDE_WMetro_dt' num2str(dt) '.png'], '-dpng','-r300');


