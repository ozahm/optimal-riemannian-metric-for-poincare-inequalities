classdef MatrixField2x2
    
    properties
        
        data % entrees of the matrices
        M % Number of matrices
        
    end
    
    
    
    methods
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        function A = MatrixField2x2(data)
            % Store a SPD matrix field as
            % 
            % Vdata = [V1(1,1), V1(1,2), V1(2,1), V1(2,2) ; 
            %          ... ;
            %          VM(1,1), VM(1,2), VM(2,1), VM(2,2)  ]
            %
            
            A.data = data;
            A.M = size(data,1);
            
        end
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        function AB = times(A,B)
            AB = A;
            AB.data = [A.data(:,1).*B.data(:,1) + A.data(:,3).*B.data(:,2) ,...
                       A.data(:,2).*B.data(:,1) + A.data(:,4).*B.data(:,2) ,...
                       A.data(:,1).*B.data(:,3) + A.data(:,3).*B.data(:,4) ,...
                       A.data(:,2).*B.data(:,3) + A.data(:,4).*B.data(:,4) ];
        end
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        function A = mtimes(A,alpha)
            A.data = A.data.*alpha;
        end
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        function A = plus(A,B)
            A.data = A.data + B.data;
        end
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        function A = inv(A)
            A = A.data;
            detA = A(:,1).*A(:,4) - A(:,2).*A(:,3);
            A = A(:,[4, 2, 3, 1]);
            A(:,[2,3]) = -A(:,[2,3]);
            A = MatrixField2x2(A./detA);
        end
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        function A = transpose(A)
            A.data(:,[3,2]) = A.data(:,[2,3]);
        end
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        function traceA = trace(A)
            traceA = A.data(:,1) + A.data(:,4);
        end
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        function EA = E(A,dmu)
            EA = A.data'*dmu;
            EA = reshape(EA,2,2);
        end
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%         function [V,D,Z] = Eigendecomposition(A,B)
            % Computes the vectors V=[v1,v2] and the eigenvalues D=[l1,l2]
            % such that
            %   A*vi = li*B*vi
            % This is equivalent to
            %   A = Z*diag(li)*Z' 
            %   B = Z*Z'
            %             A = A.data;
%             B = B.data;
%             
%             detA = A(:,1).*A(:,4) - A(:,2).*A(:,3);
%             detB = B(:,1).*B(:,4) - B(:,2).*B(:,3);
%             
%             T = B(:,1).*A(:,4) + B(:,4).*A(:,1) - B(:,2).*A(:,3)-B(:,3).*A(:,2);
%             Delta = T.^2-4*detA.*detB;
% %             Delta = max(Delta,0);
%             l1 = (T + sqrt(Delta))./(2*detB);
%             l2 = (T - sqrt(Delta))./(2*detB);
%             D = [l1,l2];
%             
%             Z1 = (A-B.*l2)./(l1-l2);
%             Z1x = sqrt(Z1(:,1));
%             Z1y = sqrt(Z1(:,4)).*sign(Z1(:,2));
%             Z2 = (A-B.*l1)./(l2-l1);
%             Z2x = sqrt(Z2(:,1)).*sign(Z2(:,2));
%             Z2y = sqrt(Z2(:,4));
%             
%             VinvT = [Z1x,Z1y,Z2x,Z2y];
%             
%             indSameEig = find( abs(l1-l2) < 1e-10 * max(abs(l1),abs(l2))  );
%             VinvT(indSameEig,:) = repmat( [1,0,0,1], length(indSameEig),1 );
%             
%             Z = MatrixField2x2(VinvT);
%             V = Z.transpose();
%             V = V.inv();
%             
%             if (~isreal(V.data))
%                     salut=1;
%             end
%       end
        function [V,D] = Eigendecomposition(A,B)
            % 
            % AV = BVD
            % with 
            %   inv(B) = V*V'
            %   A = B*V*D*V'*B
            %
            if nargin<2
                B = MatrixField2x2.eye(A.M);
            end
            
            V = MatrixField2x2.eye(A.M);
            D = MatrixField2x2.eye(A.M);
            % The "for" loop...
            for i=1:A.M
                Ai = reshape(A.data(i,:),2,2);
                Bi = reshape(B.data(i,:),2,2);
                [Vi,Di] = eig(Ai,Bi);
                V.data(i,:) = Vi(:)';
                D.data(i,:) = Di(:)';
            end
            D = D.data(:,[1,4]);
            
        end
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        function expA = exp(A)
            [V,D] = Eigendecomposition(A);
            expA = MatrixField2x2.eye(A.M);
            expA.data(:,[1,4]) = exp(D);
            expA = V.times(expA.times(V.transpose()));
        end
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        function logA = log(A)
            [V,D] = Eigendecomposition(A);
            logA = MatrixField2x2.eye(A.M);
            logA.data(:,[1,4]) = log(D);
            logA = V.times(logA.times(V.transpose()));
        end
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        function [sqrtA,sqrtAm1] = sqrt(A)
            
            [V,D] = Eigendecomposition(A);
            
%             D = max(D,1e-14);
            
            sqrtA = MatrixField2x2.eye(A.M);
            sqrtA.data(:,[1,4]) = sqrt(max(D,0));
            sqrtA = V.times(sqrtA.times(V.transpose()));
            
            sqrtAm1 = MatrixField2x2.eye(A.M);
            sqrtAm1.data(:,[1,4]) = 1./sqrt(max(D,0));
            sqrtAm1 = V.times(sqrtAm1.times(V.transpose()));
            
            % Make sure it is symmetric
            sqrtA = sqrtA.plus(sqrtA.transpose());
            sqrtA = sqrtA.mtimes(1/2);
            sqrtAm1 = sqrtAm1.plus(sqrtAm1.transpose());
            sqrtAm1 = sqrtAm1.mtimes(1/2);
        end
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        function Wt = geodesicSPD(A,B,t)
            % Wt = (B^1/2)( B^-1/2 A B^-1/2 )^(t-1)(B^1/2)
            
            if nargin<3
                t = 1/2;
            end
            
            [V,D] = Eigendecomposition(A,B);
%               inv(B) = V*V'
%               A = B*V*D*V'*B
            
            diagD = MatrixField2x2.eye(A.M);
            diagD.data(:,[1,4]) = D.^(1-t);
            BV = B.times(V);
            Wt = BV.times(diagD.times(BV.transpose()));
            
        end
%         %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%         function Wt = geodesicSPD_tangent(W,gradW,t)
%             
%             % Compute    Wt = W12*exp( t*W12*gradW*W12 )*W12
%             [U,D] = Eigendecomposition(gradW,W.inv());
%             exptD = MatrixField2x2.eye(W.M);
%             exptD.data(:,[1,4]) = exp(t*D);
%             Wt = U.times(exptD.times(U.transpose()));
%             
% %             % Compute    Wt = W12*exp( t*inv(W12)*gradW*inv(W12) )*W12
% %             [V,D] = Eigendecomposition(gradW,W);
% %             exptD = MatrixField2x2.eye(W.M);
% %             exptD.data(:,[1,4]) = exp(t*D);
% %             Wt = V.times(exptD.times(V.transpose()));
% %             Wt = W.times(Wt.times(W));
%             
%             
%             % Make sure it is symmetric
%             Wt = Wt.plus(Wt.transpose());
%             Wt = Wt.mtimes(1/2);
%             
%         end
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        function Wt = geodesicSPD_tangent(W,gradW,t,path)
            % The old implementation with W=V*V'
            
            if nargin<4
                path = 'exp';
            end
            
            switch path
                case 'VV'
                    V = W.sqrt();
                    GV = gradW.times(V);
                    Wt = GV.plus(GV.transpose());
                    Wt = V.plus( Wt.mtimes(t) );
                    Wt = Wt.times(Wt.transpose());
                    
                case 'exp'
                    % Compute    Wt = W12*exp( t*W12*gradW*W12 )*W12
                    [U,D] = Eigendecomposition(gradW,W.inv());
                    exptD = MatrixField2x2.eye(W.M);
                    exptD.data(:,[1,4]) = exp(t*D);
                    Wt = U.times(exptD.times(U.transpose()));
                    
                case 'exp_bis'
                    % Compute    Wt = W12*exp( t*inv(W12)*gradW*inv(W12) )*W12
                    [V,D] = Eigendecomposition(gradW,W);
                    exptD = MatrixField2x2.eye(W.M);
                    exptD.data(:,[1,4]) = exp(t*D);
                    Wt = V.times(exptD.times(V.transpose()));
                    Wt = W.times(Wt.times(W));
                    
                case 'exp_trunc'
                    WG = W.times(gradW);
                    Wt1 = WG.times(W.mtimes(t));
                    Wt2 = WG.times(WG.times(W.mtimes(t^2/2)));
                    Wt = W.plus( Wt1.plus( Wt2 ) );
                    
                case 'test'
                    [V,D] = Eigendecomposition(gradW,W);
                    tmp = MatrixField2x2.eye(W.M);
                    tmp.data(:,[1,4]) = 1 + t*D + (t*D).^2/2 + (t*D).^3/6;
                    Wt = V.times(tmp.times(V.transpose()));
                    Wt = W.times(Wt.times(W));
                    % AV = BVD
                    % with 
                    %   inv(W) = V*V'
                    %   G = W*V*D*V'*W
            end
            
            
            
        end
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        function W = Wof(V,OWP)
            
            switch OWP.parametrization
                case {'W'}
                    W = V;
                case {'VV', 'VV_bis'}
                    W = V.times(V.transpose());
                case 'exp'
                    W = V.exp();
                case 'VV_Cov'
                    W = MatrixField2x2( (V.data(:,1).^2)*OWP.TargetMeasure.CovMu(:)' );
                case 'VV_Id'
                    W = MatrixField2x2( (V.data(:,1).^2)*[1,0,0,1] );
                case 'exp_Cov'
                    W = MatrixField2x2( exp(V.data(:,1))*OWP.TargetMeasure.CovMu(:)' );
                case 'exp_Id'
                    W = MatrixField2x2( exp(V.data(:,1))*[1,0,0,1] );
            end
            
        end
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        function V = normalize(V,Z,OWP)
            
            switch OWP.parametrization
                case {'W'}
                    V = V.mtimes(1/Z);
                case {'VV', 'VV_bis'}
                    V = V.mtimes(1/sqrt(Z));
                case 'exp'
                    Id = MatrixField2x2.eye(V.M);
                    V = V.plus(Id.mtimes(log(1/Z)));
            end
        end
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        function dV = dVof(dW,V,OWP)
            
            switch OWP.parametrization
                case 'W'
                    dV = dW;
                    
                case 'VV'
                    VdW = V.times(dW);
                    dWV = dW.times(V);
                    dV = VdW.plus(dWV);
%                     VT = V.transpose();
%                     dV = VT.times(dW.mtimes(2));

                case 'VV_bis'
                    
                    Id = eye(2);
                    dV = dW;
                    for i=1:dW.M
                        Vi = reshape(V.data(i,:),2,2);
                        Ai = kron(Id,Vi)+kron(Vi,Id);
                        bi = dW.data(i,:)';
                        dV.data(i,:) = (Ai\bi)';
                    end
                    
%                     VdW = V.times(dW);
%                     dWV = dW.times(V);
%                     dV = VdW.plus(dWV);
%                     dV = V.times(dV.times(V));

%                     Vinv = V.inv();
%                     dV = Vinv.times(dV.times(Vinv));
                    
                    
                case 'exp'
                    [U,D] = Eigendecomposition(V);
                    UT = U.transpose();
                    dV = UT.times(dW.times(U));
                    
                    G = zeros(V.M,4);
                    epsilon = 1e-10;
                    G(:,2) = D(:,1)-D(:,2) + epsilon;
                    G(:,2) = G(:,2)./(exp(D(:,2)).*(exp(D(:,1)-D(:,2)) - 1 + epsilon ) );
                    G(:,3) = G(:,2);
                    G(:,[1,4]) = exp(-D);
                    
                    dV.data = (dV.data).*G;
                    dV = U.times(dV.times(U.transpose()));
                    
                case 'VV_Id'
                    dv = dW.data*[1;0;0;1];
                    dv = (2*dv).*V.data(:,1);
                    dV = MatrixField2x2( dv*[1,0,0,0] );
                case 'VV_Cov'
                    dv = dW.data*OWP.TargetMeasure.CovMu(:);
                    dv = (2*dv).*V.data(:,1);
                    dV = MatrixField2x2( dv*[1,0,0,0] );
                case 'exp_Cov'
                    dv = dW.data*OWP.TargetMeasure.CovMu(:);
                    dv = dv.*exp(V.data(:,1));
                    dV = MatrixField2x2( dv*[1,0,0,0] );
                case 'exp_Id'
                    dv = dW.data*[1;0;0;1];
                    dv = dv.*exp(V.data(:,1));
                    dV = MatrixField2x2( dv*[1,0,0,0] );
                    
            end
        end
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    end
    methods(Static)
        
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        function A = eye(M)
            
            data = zeros(M,4);
            data(:,[1,4]) = 1;
            A = MatrixField2x2(data);
            
        end
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        function A = zeros(M)
            
            data = zeros(M,4);
            A = MatrixField2x2(data);
            
        end
    end
end