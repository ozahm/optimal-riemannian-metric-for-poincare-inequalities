classdef TargetMeasure
    
    properties
        
        Type
        LargerSupport
        epsilon
        
        %%% Geometry
        Model
        GeometryParameters
        indexSupport
        
        %%% Mesh
        nElem
        nNodes
        ElementCenter
        ElementSurface
        node2element
        element2node
        t
        p
        
        %%% Target measure
        mu
        Mmu
        MeanMu
        CovMu
        CovMu12
        
    end
    
    
    methods
        
        function obj = TargetMeasure(type,LargerSupport,h,hmax)
            
            if nargin<1
                type = 'H';
            end
            if nargin<2
                LargerSupport = 0;
            end
            if nargin<3
                h = 0.2;
            end
            
            obj.Type = type;
            obj.LargerSupport = LargerSupport;
            obj = obj.initGeometry(h);
            obj = initMesh(obj,hmax);
            if LargerSupport<0
                obj.epsilon = -LargerSupport;
            else
                obj.epsilon = 0;
            end
            % Create target density and normalize it
            obj.mu = zeros(obj.nElem,1);
            switch obj.Type
                case {'H','T','O','X','Square'}
                    % Uniform measure
                    obj.mu( findElements(obj.Model.Mesh,"region","Face",obj.indexSupport) ) = 1;
                otherwise
                    % Evaluate the pdf
                    obj.mu = obj.pdf( obj.ElementCenter' );
            end
            
            % Add epsilon to the object to avoid singularities....
            obj.mu = obj.mu + obj.epsilon;
            
            Z = obj.ElementSurface*obj.mu;
            obj.mu = obj.mu/Z;
            
            % Create the mass matrix and normalize mu
            obj.Mmu = obj.AssembleMassMatrix();
            Z = sum(sum(obj.Mmu));
            obj.Mmu = obj.Mmu/Z;
            
            
            % Compute the covariance of mu
            obj = obj.MomentsMu();
            
        end
        
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %       GEOMETRY
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        function obj = initGeometry(obj,h)
            
            obj.Model = createpde;
            
            obj.GeometryParameters = [];
            obj.GeometryParameters.h = h;
            
            switch obj.Type
                case 'O'
                    obj = obj.Geometry_O();
                case 'X'
                    obj = obj.Geometry_X();
                case 'T'
                    obj = obj.Geometry_T();
                case 'H'
                    obj.GeometryParameters.L = 1;
                    obj.GeometryParameters.H = 1;
                    obj.GeometryParameters.l = 0.2;
                    obj = obj.Geometry_H();
                otherwise
                    obj = obj.Geometry_Square();
                    obj.GeometryParameters = [];
            end
            
            
        end
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        function obj = Geometry_O(obj)
            
            L = 1;
            l = 1-obj.GeometryParameters.h;
            
            C1 = [1;0;0;L];
            C2 = [1;0;0;l];
            
            % Create the geometry
            gd = [C1,C2];
            sf = 'C1-C2';
            ns = char('C1','C2');
            obj.indexSupport =  1;
            
            % If larger support
            if obj.LargerSupport > 0
                coef = 1+obj.LargerSupport;
                R0 = [3,4,-coef*L,-coef*L,coef*L,coef*L,-coef*L,coef*L,coef*L,-coef*L]';
                gd = [[gd;zeros(10-4,2)],R0];
                sf = [sf '+R0'];
                ns = char(ns,'R0');
                obj.indexSupport = 3;
            elseif obj.LargerSupport < 0
                % Convexe hull
                sf = 'C1+C1';
                obj.indexSupport = 2;
            end
            [dl,bt] = decsg(gd,sf,ns');
            geometryFromEdges(obj.Model,dl);
            
            
        end
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        function obj = Geometry_X(obj)
            
            h = obj.GeometryParameters.h;
            h = h/(2*sqrt(2));
            
            R1 = [3,4,-1+h,-1-h,1-h,1+h,-1-h,-1+h,1+h,1-h]';
            R2 = [3,4,1-h,-1-h,-1+h,1+h,-1-h,1-h,1+h,-1+h]';
            
            % Create the geometry
            gd = [R1,R2];
            sf = 'R1+R2';
            ns = char('R1','R2');
            
            % If larger support
            if obj.LargerSupport == 0
                [dl,bt] = decsg(gd,sf,ns');
                [dl,bt] = csgdel(dl,bt);
                obj.indexSupport = 1;
            elseif obj.LargerSupport > 0
                coef = 1+obj.LargerSupport;
                L = (1+h)*coef;
                R0 = [3,4,-L,-L,L,L,-L,L,L,-L]';
                gd = [gd,R0];
                sf = [sf '+R0'];
                ns = char(ns,'R0');
                
                [dl,bt] = decsg(gd,sf,ns');
                [dl,bt] = csgdel(dl,bt,[8,17,14,11]);
                obj.indexSupport = 1;
            elseif obj.LargerSupport < 0
                
                
                R0 = [2,8,-1-h,-1-h,-1+h,1-h,1+h,1+h,1-h,-1+h,...
                          -1+h,1-h,1+h,1+h,1-h,-1+h,-1-h,-1-h]';
                gd = [[gd;zeros(8,2)],R0];
                sf = [sf '+R0'];
                ns = char(ns,'R0');
                
                [dl,bt] = decsg(gd,sf,ns');
                [dl,bt] = csgdel(dl,bt,[10,13,16,19]);
                obj.indexSupport = 5;
                
                
            end
            
            % Include the geometry in the model and plot it.
            geometryFromEdges(obj.Model,dl);
            
        end
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        function obj = Geometry_T(obj)
            
            h = obj.GeometryParameters.h;
            R1 = [3,4,-1,-1,1,1,1-2*h,1,1,1-2*h]';
            R2 = [3,4,-h,-h,h,h,-1,1-h,1-h,-1]';
            
            % Create the geometry
            gd = [R1,R2];
            sf = 'R1+R2';
            ns = char('R1','R2');
            
            % If larger support
            if obj.LargerSupport == 0
                [dl,bt] = decsg(gd,sf,ns');
                [dl,bt] = csgdel(dl,bt);
                obj.indexSupport = 1;
            elseif obj.LargerSupport > 0
                coef = 1+obj.LargerSupport;
                L = coef;
                R0 = [3,4,-L,-L,L,L,-L,L,L,-L]';
                gd = [gd,R0];
                sf = [sf '+R0'];
                ns = char(ns,'R0');
                
                [dl,bt] = decsg(gd,sf,ns');
                [dl,bt] = csgdel(dl,bt,[12,8,3,10]);
                obj.indexSupport = 1;
            elseif obj.LargerSupport < 0
                L = 1;
                R0 = [2,6,-1,-1,1,1,h,-h,...
                         1-2*h,1,1,1-2*h,-1,-1]';
                gd = [[gd;zeros(4,2)],R0];
                sf = [sf '+R0'];
                ns = char(ns,'R0');
                
                [dl,bt] = decsg(gd,sf,ns');
                [dl,bt] = csgdel(dl,bt,[9,12,1,13]);
                obj.indexSupport = 1;
                
            end
            
            % Include the geometry in the model and plot it.
            geometryFromEdges(obj.Model,dl);
            
        end
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        function obj = Geometry_H(obj)
            
            L = obj.GeometryParameters.L;
            H = obj.GeometryParameters.H;
            h = obj.GeometryParameters.h;
            l = obj.GeometryParameters.l;
            
            % Create the rectangles
            R1 = [3,4,-L/2,-L/2,-l/2,-l/2,-H/2,H/2,H/2,-H/2]';
            R2 = [3,4,l/2,l/2,L/2,L/2,-H/2,H/2,H/2,-H/2]';
            R3 = [3,4,-l/2,-l/2,l/2,l/2,-h/2,h/2,h/2,-h/2]';
            
            % Create the geometry
            gd = [R1,R2,R3];
            sf = 'R1+R2+R3';
            ns = char('R1','R2','R3');
            
            % If larger support
            if obj.LargerSupport == 0
                [dl,bt] = decsg(gd,sf,ns');
                [dl,bt] = csgdel(dl,bt);
                obj.indexSupport = 1;
            elseif obj.LargerSupport > 0
                coef = 1+obj.LargerSupport;
                R4 = [3,4,-L/2*coef,-L/2*coef,L/2*coef,L/2*coef,-H/2*coef,H/2*coef,H/2*coef,-H/2*coef]';
                gd = [gd,R4];
                sf = [sf '+R4'];
                ns = char(ns,'R4');
                
                [dl,bt] = decsg(gd,sf,ns');
                [dl,bt] = csgdel(dl,bt,[9,12]);
                obj.indexSupport = 1;
            elseif obj.LargerSupport < 0
                
                coef = 1;
                R4 = [3,4,-L/2*coef,-L/2*coef,L/2*coef,L/2*coef,-H/2*coef,H/2*coef,H/2*coef,-H/2*coef]';
                gd = [gd,R4];
                sf = [sf '+R4'];
                ns = char(ns,'R4');
                
                [dl,bt] = decsg(gd,sf,ns');
                [dl,bt] = csgdel(dl,bt,[4,7]);
                obj.indexSupport = 1;
                
            end
            
            % Include the geometry in the model and plot it.
            geometryFromEdges(obj.Model,dl);
            
        end
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        function obj = Geometry_Square(obj)
            
            % Create the geometry
            gd = [3,4,-1,-1,1,1,-1,1,1,-1]';
            sf = 'R1';
            ns = char('R1');
            [dl,bt] = decsg(gd,sf,ns');
            geometryFromEdges(obj.Model,dl);
            obj.indexSupport = 1;
            
        end%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        function W = Fisher(obj)
            X = obj.ElementCenter';
            W = MatrixField2x2.eye(size(X,1));
            switch obj.Type
                case 'DoubleBanana_DIRT'
                    a = 100;
                    sigma = 1;
                    scale_x = 3;
                    scale_y = 3;
                    shift_y = 0.2;
                    %
                    x = X(:,1);
                    y = X(:,2);
                    x = x * scale_x;
                    y = ( y + shift_y ) * scale_y;
                    %
                    tmp = a*(-x.^2+y).^2 + (x-1).^2;
                    J = [-(4*a*x.*(-x.^2+y)-2*x+2)./tmp, ...
                        (a*(-2*x.^2+2*y))./tmp];
                    J(:,1) = J(:,1)*scale_x;
                    J(:,2) = J(:,2)*scale_y;
                    for i = 1:size(X,1)
                        I = inv(J(i,:)'*(J(i,:)/sigma^2)+eye(2));
                        %I = J(i,:)'*(J(i,:)/sigma^2)+eye(2);
                        W.data(i,:) = I(:);
                    end
            end
        end
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        function f = pdf(obj,X)
            x = X(:,1);
            y = X(:,2);
            switch obj.Type
                case 'Banana'
                    sigma = 0.2;
                    f = (y-2*x.^2+0.75).^2*10 + sigma*x.^2 + y.^2 ;
                    f = exp(-f/sigma);
                case 'DoubleBanana'
                    % sigma = 0.2;
                    sigma = 0.3;
                    a = sigma;
                    f1 = (y-2*x.^2+0.75).^2*10 + a*x.^2 ;
                    f2 = (y-2*x.^2-0.25).^2*10 + a*x.^2 ;
                    f = exp(-f1/sigma) + exp(-f2/sigma);
                case 'DoubleBanana_bis'
                    sigma = 0.2;
                    a = 0.5;
                    f1 = (y-2*(x+0.25).^2+0.75).^2*10 + a*(x+0.25).^2 ;
                    f2 = (-y-2*(x-0.25).^2+0.75).^2*10 + a*(x-0.25).^2 ;
                    f = exp(-f1/sigma) + exp(-f2/sigma);
                case 'DoubleBanana_DIRT'
                    a = 100;
                    sigma = 1;
                    data = 5;
                    scale_x = 3;
                    scale_y = 3;
                    shift_y = 0.2;
                    %
                    x = x * scale_x;
                    y = ( y + shift_y ) * scale_y;
                    %
                    tmp = a*(-x.^2+y).^2 + (x-1).^2;
                    z = log(tmp);
                    %
                    f = -0.5*(z-data).^2/sigma^2-0.5*(x.^2+y.^2);
                    f = exp(f);
                case 'Trimodal'
%                     sigma = 0.01;
                    sigma = 0.025;
%                     sigma = 0.1;
                    r = 0.5;
                    f1 = (x).^2 + (y-r).^2;
                    f2 = (x-r*sqrt(3)/2).^2 + (y+r*1/2).^2;
                    f3 = (x+r*sqrt(3)/2).^2 + (y+r*1/2).^2;
                    f = exp(-f1/sigma) + exp(-f2/sigma) + exp(-f3/sigma);
                    
                case 'MultiModal'
                    sigma = 0.01;
                    
                    f1 = (x).^2 + (y).^2;
                    f2 = (x-1/2).^2 + (y-1/2).^2;
                    f3 = (x+1/2).^2 + (y-1/2).^2;
                    f4 = (x-1/2).^2 + (y+1/2).^2;
                    f5 = (x+1/2).^2 + (y+1/2).^2;
                    f6 = (x).^2 + (y+0.7).^2;
                    f = exp(-f1/sigma) + exp(-f2/sigma) + exp(-f3/sigma) + exp(-f4/sigma) + exp(-f5/sigma) + exp(-f6/sigma);
                    
                case 'Laplace'
                    
                    sigma = 0.5;
                    f = abs(x)+abs(y);
                    f = exp(-f/sigma);
                    
                case 'doubleSquare'
                    
                    l = 0.5;
                    f = ones(size(x));
                    ind = ((-1+l)<=x)&(x<=(1-l)) & ((-1+l)<=y)&(y<=(1-l));
                    f(ind) = 0.1;
                    
                case 'Ring'
                    
                    sigma = 0.04;
                    r = sqrt(x.^2+y.^2);
                    f = exp(-(r-0.65).^2/(2*sigma^2));
                    
                case 'Star'
                    
                    
                    sigma = 0.03;
                    r = sqrt(x.^2+y.^2);
                    theta = atan(y./(x+eps)).*sign(x+eps);
                    f = exp(-( r-0.75 ).^2/(2*sigma^2)).*(1-exp( -(theta-pi/2).^4 ));
                    f = f + 1E-6;
                    
                otherwise
                    error('Not implemented')
            end
        end
        
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        function obj = initMesh(obj,hmax)
            
            % Generate the mesh
            generateMesh(obj.Model,"Hmax",hmax,"GeometricOrder","linear");
            % pdemesh(obj.Model)
            
            % Compute nElem, nNodes, ElementSurface and ElementCenter
            obj.nElem = size(obj.Model.Mesh.Elements,2);
            obj.nNodes = size(obj.Model.Mesh.Nodes,2);
            [~,obj.ElementSurface] = area(obj.Model.Mesh);
            obj.ElementCenter = zeros(2,obj.nElem);
            for e=1:obj.nElem
                barycenter = obj.Model.Mesh.Nodes(:,obj.Model.Mesh.Elements(:,e));
                obj.ElementCenter(:,e) = sum(barycenter,2)/3;
            end
            
            elements = obj.Model.Mesh.Elements;
            j=[];
            i=[];
            s=[];
            for n=1:obj.nNodes
                elemSharingNode = ceil(find(elements(:)==n)/3);
                j = [j;elemSharingNode];
                i = [i;n*ones(length(elemSharingNode),1)];
                sn = obj.ElementSurface(elemSharingNode);
                sn = sn/sum(sn);
                s = [s;sn(:)];
            end
            obj.element2node = sparse(i,j,s,obj.nNodes,obj.nElem);
            j = elements(:);
            i = repmat([1:obj.nElem],3,1);
            s = 1/3*ones(3*obj.nElem,1);
            obj.node2element = sparse(i(:),j,s,obj.nElem,obj.nNodes);
            
            [obj.p,~,obj.t] = meshToPet(obj.Model.Mesh);
            
        end
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        
        
        
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        % TARGET MEASURE
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        function Mass = AssembleMassMatrix(obj,nu)
            % Normalize the density mu (elementswise) and compute Emu and 
            % Mmu such that
            %       Emu*v = int v dmu
            %       u'*Mmu*v = int uv dmu
            % for any node-field functions u,v
            
            if nargin == 1
                nu = obj.mu;
            end
            [Mass,~,~,~] = assempde(obj.Model,0,nu',0);
            Z = sum(sum(Mass));
            Mass = Mass/Z;
        end
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        function obj = MomentsMu(obj)
            % Compute the mean and covariance of mu
            
            f_linear = obj.Model.Mesh.Nodes';
            f_one = ones(size(f_linear,1),1);
            obj.MeanMu = (f_one'*obj.Mmu)*f_linear;
            obj.CovMu = (f_linear-obj.MeanMu)'*obj.Mmu*(f_linear-obj.MeanMu);
            
            [UU,DD,~] = svd(obj.CovMu);
            Cov12 = UU*diag(diag(DD).^(1/2))*UU';
            Cov12 = repmat(Cov12(:)',obj.nElem,1);
            obj.CovMu12 = MatrixField2x2(Cov12);
        end
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        function [varf,meanf] = var(obj,f,nu)
            if nargin<3
                Mass = obj.Mmu;
            else
                Mass = obj.AssembleMassMatrix(nu);
            end
            if size(f,1)==obj.nElem
                f = obj.element2node*f;
            end
            
            meanf = sum(Mass*f);
            f = f - meanf;
            varf = f'*Mass*f;
        end
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        function K = Stiffness(obj,K)
            % %% Create mass/stiffness matrix
            % % https://fr.mathworks.com/help/pde/ug/specify-nonconstant-pde-coefficients.html
            % % http://matrix.etseq.urv.es/manuals/matlab/toolbox/pde/assempde.html
            
            if isa(K,'double')
                Id = MatrixField2x2.eye(obj.nElem);
                K = Id.mtimes(K(:));
            end
            
            K = K.mtimes(obj.mu);
            [K,~,~,~] = assempde(obj.Model,K.data',0,0);
            
        end
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        % FIELDS
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        function [gradu] = gradientOfField(obj,u)
            
            [gradx,grady] = pdegrad(obj.p,obj.t,u);
            gradu = [gradx(:),grady(:)];
        end
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        function drift = drift(obj,W)
            
            if nargin<2 || isempty(W)
                W = MatrixField2x2.eye(obj.nElem);
            end
            
            Wnode = W;
            Wnode.data = obj.element2node*W.data;
            
            [W11x,~] = pdegrad(obj.p,obj.t,Wnode.data(:,1));
            [~,W12y] = pdegrad(obj.p,obj.t,Wnode.data(:,2));
            [W21x,~] = pdegrad(obj.p,obj.t,Wnode.data(:,3));
            [~,W22y] = pdegrad(obj.p,obj.t,Wnode.data(:,4));
            
            divWx = W11x(:) + W12y(:);
            divWy = W21x(:) + W22y(:);
            
            % Compute Wgradlogmu
            [logmux,logmuy] = pdegrad(obj.p,obj.t,obj.element2node*log(obj.mu));
            Wgradlogmux = W.data(:,1).*logmux(:) + W.data(:,3).*logmuy(:);
            Wgradlogmuy = W.data(:,2).*logmux(:) + W.data(:,4).*logmuy(:);
            
            % Assemble the drift
            drift = [ divWx + Wgradlogmux , divWy + Wgradlogmuy];
            
        end
        
        
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %       PLOT
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        function obj = plotGeo(obj)
            pdegplot(obj.Model,"EdgeLabels","on","FaceLabels","on")
        end
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        function plotMesh(obj)
            pdemesh(obj.Model)
            colorbar off
            axis([min(obj.Model.Mesh.Nodes(1,:)), max(obj.Model.Mesh.Nodes(1,:)), min(obj.Model.Mesh.Nodes(2,:)), max(obj.Model.Mesh.Nodes(2,:))])
            axis equal
            set(gca, 'box','off','XTickLabel',[],'XTick',[],'YTickLabel',[],'YTick',[])
            axis off
        end
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        function plot(obj,data,colorbarFlag)
            if nargin<3
                colorbarFlag=0;
            end
            
            pdeplot(obj.Model,'XYData',data,'Contour','on'); % , 'ColorMap','cool(8)'
            if ~colorbarFlag
                colorbar off
            end
%             if length(data)==obj.nNodes
%                 pdeplot(obj.Model,'XYData',data,'Contour','on');
%             elseif length(data)==obj.nElem
%                 plotElementField(obj,data);
%             end
            axis([min(obj.Model.Mesh.Nodes(1,:)), max(obj.Model.Mesh.Nodes(1,:)), min(obj.Model.Mesh.Nodes(2,:)), max(obj.Model.Mesh.Nodes(2,:))])
%             axis equal
%             set(gca,'XTick',[])
%             set(gca,'YTick',[])
            set(gca, 'box','off','XTickLabel',[],'XTick',[],'YTickLabel',[],'YTick',[])
            axis off
        end
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        function h = plotElementField(obj,data)
            
            tmp = repmat(data(:)',3,1);
            [p,e,t] = meshToPet(obj.Model.Mesh);
            t = t(1:3,:);
            x = p(1,:);
            y = p(2,:);
            P = [x(t(:));y(t(:))];
            T = reshape(1:size(P,2),[3 size(P,2)/3]);
            h = trisurf(T',P(1,:),P(2,:),tmp(:));
            set(h,'edgecolor','none');
            h.LineWidth=0.01;
            
            grid off
            view(0,90)
            
        end
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        function plotW(obj,W,N,r)
            
            if nargin<2 || isempty(W)
                W = MatrixField2x2.eye(obj.nElem);
%                 W.data(:,[2,3]) = 1e-8;
%                 W = W.mtimes(0.1);
            end
            if nargin<3
                N = 30;
            end
            if nargin<4
                r = 0.03;
            end
            % Interpolate the drift/divW on a regular grid
            
            XminYmin = min(obj.Model.Mesh.Nodes');
            XmaxYmax = max(obj.Model.Mesh.Nodes');
            X = linspace(XminYmin(1),XmaxYmax(1),N);
            Y = linspace(XminYmin(2),XmaxYmax(2),N);
            [X,Y] = meshgrid(X,Y);
            querypoints = [X(:),Y(:)]';
            
            W11 = interpolateSolution(createPDEResults(obj.Model,obj.element2node*W.data(:,1)),querypoints);
            W12 = interpolateSolution(createPDEResults(obj.Model,obj.element2node*W.data(:,2)),querypoints);
            W21 = interpolateSolution(createPDEResults(obj.Model,obj.element2node*W.data(:,3)),querypoints);
            W22 = interpolateSolution(createPDEResults(obj.Model,obj.element2node*W.data(:,4)),querypoints);
            
            Wgrid = MatrixField2x2([W11(:),W12(:),W21(:),W22(:)]);
            [V,D] = Wgrid.Eigendecomposition();
            % Normalize...
            D = (D+0.01).^0.5;
            D = D./sqrt(sum(D.^2,2));
            

            % Plot the trace 
            obj.plot(log(trace(W))/log(10),1)
            hold on
            
            % Plot the ellipses
            color = [0.9,0.9,0.9];
            t = -pi:0.2:pi;
            circle = r*[cos(t); sin(t)];
            for i=1:N^2
%                 ell = [X(i);Y(i)]+ [V1x(i),V2x(i);V1y(i),V2y(i)]*diag([D1(i),D2(i)]) * circle;
                ell = [X(i);Y(i)]+ [V.data(i,1),V.data(i,3);V.data(i,2),V.data(i,4)]*diag([D(i,1),D(i,2)]) * circle;
                patch(ell(1,:),ell(2,:),color);
            end
            axis([XminYmin(1),XmaxYmax(1),XminYmin(2),XmaxYmax(2)])
            
            hold off
        end
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        function plotDrift(obj,W,N)
            
            if nargin<2 || isempty(W)
                W = MatrixField2x2.eye(obj.nElem);
            end
            
            % Interpolate the drift/divW on a regular grid
            if nargin<3
                N = 30;
            end
            XminYmin = min(obj.Model.Mesh.Nodes');
            XmaxYmax = max(obj.Model.Mesh.Nodes');
            X = linspace(XminYmin(1),XmaxYmax(1),N);
            Y = linspace(XminYmin(2),XmaxYmax(2),N);
            [X,Y] = meshgrid(X,Y);
            querypoints = [X(:),Y(:)]';
            
            drift = obj.drift(W);
            
            drift_x = interpolateSolution(createPDEResults(obj.Model,obj.element2node*drift(:,1)),querypoints);
            drift_y = interpolateSolution(createPDEResults(obj.Model,obj.element2node*drift(:,2)),querypoints);
            
%             normDrift = sqrt(drift_y.^2+drift_x.^2);
%             drift_x = drift_x./normDrift;
%             drift_y = drift_y./normDrift;
            
            obj.plot(obj.mu)
            hold on
            
%             % Mask the drift where it is small...
%             n = sqrt(drift_x.^2 + drift_y.^2);
%             drift_x = drift_x.*( n>min(n)*(max(n)/min(n))^0.3 );
%             drift_y = drift_y.*( n>min(n)*(max(n)/min(n))^0.3 );
            l = streamslice(X,Y,reshape(drift_x,N,N),reshape(drift_y,N,N) ,0.75, 'method','cubic');
            set(l,'LineWidth',2)
            set(l,'Color','k');
            
%             quiver( X(:), Y(:), drift_x(:), drift_y(:), 'Color','k');

            hold off
        end
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        function X = Langevin(obj,X0,N,dt,W)
            
            if nargin<5 || isempty(W)
                W = MatrixField2x2.eye(obj.nElem);
                W = W.mtimes(trace(obj.CovMu)/2);
            end
            D = obj.drift(W);
            Dx = createPDEResults(obj.Model,obj.element2node*D(:,1));
            Dy = createPDEResults(obj.Model,obj.element2node*D(:,2));
            
            B = W.sqrt();
            B = B.mtimes(sqrt(2));
            B11 = createPDEResults(obj.Model,obj.element2node*B.data(:,1));
            B12 = createPDEResults(obj.Model,obj.element2node*B.data(:,2));
            B21 = createPDEResults(obj.Model,obj.element2node*B.data(:,3));
            B22 = createPDEResults(obj.Model,obj.element2node*B.data(:,4));
            
            X = zeros(N,2);
            X(1,:) = X0(:);
            
            reflection = @(t) ((-1).^floor((t+1)/2+1)) + (-1).^floor((t+1)/2).*mod(t+1,2) ;
%             reflection = @(t) reflection(t/2)*2;
            for n = 2:N
                
                x = X(n-1,:);
                
                driftx1 = interpolateSolution(Dx, x'  );
                driftx2 = interpolateSolution(Dy, x'  );
                Bx11 = interpolateSolution(B11, x'  );
                Bx12 = interpolateSolution(B12, x'  );
                Bx21 = interpolateSolution(B21, x'  );
                Bx22 = interpolateSolution(B22, x'  );
                
                Bx = [Bx11,Bx12;Bx21,Bx22];
                drift = [driftx1;driftx2];
                
                X(n,:) = x + dt*drift' + randn(1,2)*(sqrt(dt)*Bx);
                X(n,:) = reflection(X(n,:));
                
            end
            
        end

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        function X = Langevin_MH(obj,X0,N,dt,W)
            
            if nargin<5 || isempty(W)
                W = MatrixField2x2.eye(obj.nElem);
                W = W.mtimes(trace(obj.CovMu)/2);
            end
            D = obj.drift(W);
            Dx = createPDEResults(obj.Model,obj.element2node*D(:,1));
            Dy = createPDEResults(obj.Model,obj.element2node*D(:,2));
            
            B = W.sqrt();
            B = B.mtimes(sqrt(2));
            B11 = createPDEResults(obj.Model,obj.element2node*B.data(:,1));
            B12 = createPDEResults(obj.Model,obj.element2node*B.data(:,2));
            B21 = createPDEResults(obj.Model,obj.element2node*B.data(:,3));
            B22 = createPDEResults(obj.Model,obj.element2node*B.data(:,4));
            
            X = zeros(N,2);
            X(1,:) = X0(:);
            
            reflection = @(t) ((-1).^floor((t+1)/2+1)) + (-1).^floor((t+1)/2).*mod(t+1,2) ;
%             reflection = @(t) reflection(t/2)*2;

            % current x and drift
            x = X(1,:);
            %
            driftx1 = interpolateSolution(Dx, x'  );
            driftx2 = interpolateSolution(Dy, x'  );
            Bx11 = interpolateSolution(B11, x'  );
            Bx12 = interpolateSolution(B12, x'  );
            Bx21 = interpolateSolution(B21, x'  );
            Bx22 = interpolateSolution(B22, x'  );
            %
            Bx = [Bx11,Bx12;Bx21,Bx22];
            drift = [driftx1;driftx2];
            phix = log(pdf(obj,x));

            i = 0;
            for n = 2:N
                r = randn(2,1);
                xnew = x + dt*drift' + (sqrt(dt)*Bx*r)';
                xnew = reflection(xnew);
                %{
                xtest = reflection(xnew);
                i = 0;
                while norm(xtest-xnew) > 1e-6
                    r = randn(2,1);
                    xnew = x + dt*drift' + (sqrt(dt)*Bx*r)';
                    xtest = reflection(xnew);
                    i = i + 1;
                end
                if i > 0
                    disp(i)
                end
                %}
                % drift at xnew
                driftxn1 = interpolateSolution(Dx, xnew'  );
                driftxn2 = interpolateSolution(Dy, xnew'  );
                Bxn11 = interpolateSolution(B11, xnew'  );
                Bxn12 = interpolateSolution(B12, xnew'  );
                Bxn21 = interpolateSolution(B21, xnew'  );
                Bxn22 = interpolateSolution(B22, xnew'  );
                %
                Bxnew = [Bxn11,Bxn12;Bxn21,Bxn22];
                driftnew = [driftxn1;driftxn2];
                phixnew = log(pdf(obj,xnew));

                % compute the log acceptance probability
                rnew = - Bxnew\(sqrt(dt)*(drift+driftnew) + Bx*r);
                log_prop = - log(det(Bx)) - 0.5*norm(r)^2;
                log_prop_new = - log(det(Bxnew)) - 0.5*norm(rnew)^2;

                alpha = phixnew - phix + log_prop_new - log_prop;

                %[log(det(Bx)) log(det(Bxnew)) alpha]

                if alpha > log(rand) % accept
                    x = xnew;
                    Bx = Bxnew;
                    drift = driftnew;
                    phix = phixnew; 
                    i = i + 1;
                end
                X(n,:) = x;
            end
            i/n 
        end
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    end
end